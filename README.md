ParticlesWizard is a particle flow data analyzer/parser for MRI simulation of 
blood flow. It converts flow trajectory data from Comsol software to particle 
motion data required by the MRI simulator.

ParticlesWizard is licensed under the GNU GENERAL PUBLIC LICENSE.

This software is developed in frameworks of the research project:
"A Computer SIMulator of Magnetic Resonance Angiography Imaging for Validating 
Methods of Quantitative Analysis of the Vascular System"
Web page: http://www.eletel.p.lodz.pl/angiosim/
The project is supported by the Polish National Science Centre Grant 
No. 6509/B/T02/2011/40

ParticlesWizard was developed with Qt Creator IDE. It depends on Qt and qhull 
library. The source codes of the libqhull and the license are available from 
http://www.qhull.org/. The Qt and its license are available from 
http://qt-project.org/

The ParticlesWizard project results were published in: 

Klepaczko, A., Szczypiński, P., Dwojakowski, G., Strzelecki, M., & Materka, A. 
(2014). Computer simulation of magnetic resonance angiography imaging: model 
description and validation. PloS one, 9(4)

Klepaczko, A., Materka, A., Szczypinski, P., & Strzelecki, M. 
(2015). Numerical Modeling of MR Angiography for Quantitative Validation of 
Image-Driven Assessment of Carotid Stenosis. Nuclear Science, IEEE Transactions 
on, 62(3), 619-627